---
title: "Velká mapa rodáků: kde jsou lidé věrní své obci"
perex: "Nejvěrnější obyvatele má obec Strání u Uherského Hradiště. Čtyři z pěti lidí, kteří ve Strání žijí, se v této vesnici také narodili. Naopak nejvyšší podíl přespolních najdeme v obcích, kde sídlí věznice."
description: "Nejvěrnější obyvatele má obec Strání u Uherského Hradiště, žije tu skoro 80 procent rodáků. Naopak nejvíc přespolních mají obce, kde sídlí nejrůznější věznice."
authors: ["Jan Cibulka, Petra Holinková", "Karolína Peřestá"]
published: "19. dubna 2017"
coverimg: https://interaktivni.rozhlas.cz/rodaci/media/cover.jpg
coverimg_note: "Lidé jsou hrdí na to, že jsou Straňáci. Je to vidět i na tom, jak se starají o své domy, říká starosta obce s nejvyšším podílem rodáků Foto archiv obce Strání</a>"
socialimg: https://interaktivni.rozhlas.cz/rodaci/media/socimg.jpg
url: "rodaci"
# styles: []
# libraries: [d3, topojson, jquery, highcharts, leaflet, inline-audio]
libraries: [jquery]
recommended:
  - link: https://interaktivni.rozhlas.cz/sudety/
    title: Existují Sudety? Hranice jsou zřetelné i sedmdesát let po vysídlení Němců
    perex: Statistiky nezaměstnanosti, kvality vzdělání nebo volební účasti v Česku dodnes kopírují hranice historických Sudet. Vlnu osadníků připomíná také nejsilnější příhraniční menšina – Slováci.
    image: 	https://interaktivni.rozhlas.cz/sudety/media/e9bae3cb82a75d9f64058b3dc984789a/1920x_.jpg
---

Slovácko a Vysočina, to jsou dvě oblasti, kde lidé nejčastěji žijí ve své rodné obci a opustí ji jen v necelé třetině případů. Na Chomutovsku a Mostecku je poměr opačný. Podle demografů má na podíl rodáků v obcích vliv několik faktorů: dostupnost práce, vzdělání a kvalitního bydlení, ale také poválečné vysídlení Němců, trend stěhování lidí z center velkých měst do jejich zázemí a zřejmě i vztah lidí k místním tradicím a náboženství. 

<!---
<div data-bso="1"></div>
-->

_Detaily všech obcí si můžete prohlédnout v následující mapě. Čím sytější barva, tím větší podíl rodáků v obci. Pokud na obec najedete myší, zobrazí se podíl rodáků na jednotlivých věkových kategoriích._

<aside class="big">
  <iframe src="https://interaktivni.rozhlas.cz/data/rodaci-mapa/" height=700></iframe>
</aside>

To, že na rozhodnutí zůstat v rodné obci má velký vliv cena a kvalita bydlení, potvrdil Českému rozhlasu starosta Šitbořic na Břeclavsku, jedné z obcí s nejvyšším podílem rodáků. _„Je tady slušná občanská vybavenost, takže lidé nemají důvod se stěhovat do měst. Také připravujeme neustále nová stavební místa mladým, aby nemuseli shánět parcely někde jinde,“_ říká starosta Šitbořic Antonín Lengál.

Stavební pozemky obec prodává už pouze místním, a to za dotovanou cenu. _„Směrem k Brnu se prodávají pozemky za 1000 až 1500 korun, my jim naše pozemky prodáváme za necelou pětistovku. Zájem je obrovský, připravujeme teď další lokalitu s 24 místy na prodej a během týdne už je z toho více než polovina obsazených,“_ vysvětluje Lengál.

<aside class="small">
Podíl rodáků v obci není jediná číslo, která popisuje stabilitu obyvatelstva, podstatný je i počet vystěhovaných (z obce, odkud všichni jen utíkají, a nikdo nepřichází, by byl podíl rodáků také vysoký). Trend je ale v Česku u obou jevů obdobný: lidé odcházejí hlavně z obcí v někdejších Sudetech, tedy z míst, kde je rodáků méně.<br>

<img src="https://interaktivni.rozhlas.cz/rodaci/media/vysteh.png"><br>

<i>Zdroj <a target="_blank" href="https://www.czso.cz/csu/sldb">SLDB 2011</a>, sytá barva značí větší podíl vystěhovaných</i>
</aside>

Zvýhodněné stavební pozemky se před třemi týdny rozhodlo mladým párům nabídnout také zastupitelstvo Strání. _„Cena bude poloviční,“_ říká starosta Antonín Popelka. Na 23 připravovaných parcel už má obec 16 vážných zájemců, kde aspoň jeden z páru je místním obyvatelem.

Podle Popelky ale za vysoký počet rodáků zůstávajících v obci může hlavně místní patriotismus i mezigenerační propojení lidí například v jednom z 22 kulturních spolků. Obec se snaží nabídnout občanům i sportovní vyžití: _„Fotbal, tenis, volejbal... Máme tady všechno,“_ vypočítává Popelka.

Ve Strání jsou dokonce dvě školky, jako spádová obec má až 800 míst na základní škole. Hodně lidí podle starosty jezdí přes týden za prací do Prahy, Brna i za hranice, ale na víkend se vracejí. _„Lidé jsou hrdí na to, že jsou Straňáci. Je to vidět i na tom, jak se starají o své domy,“_ uzavírá.

Starostova slova potvrzuje i jeho předchůdce ​Ondřej Benešík: _„My jsme tady prostě, jak říkáme, za kopcem. My jsme tady izolovaní. Myslím si, že hraje velkou roli vysoká religiozita, která drží tu obec pohromadě,“_ dodal pro Český rozhlas.

## Nejvyšší podíl rodáků má Zlínský kraj

Podle studie [Českého statistického úřadu](https://www.czso.cz/csu/czso/obyvatelstvo-ceske-republiky-podle-mista-narozeni-2011-hj3iacbvta) tvoří rodáci v průměru polovinu obyvatel obcí. Větší obce mají obecně podíl rodáků větší, protože místní se sice stěhují, ale často zůstanou na území stejného města. Naopak nejmenší obce mají rodáků v průměru pod 45 %.

_„V moravských krajích (s výjimkou Olomouckého) a v Praze představují osoby narozené v obci bydliště více než polovinu obyvatel, nejvyšší podíl rodáků má Zlínský kraj – 53 %. Oproti tomu ve většině českých krajů nepřesahuje podíl rodáků 45 %, vůbec nejnižší je v Karlovarském a Středočeském kraji, kde nedosahuje ani 40 %,“_ uvádějí statistici.

Statistický úřad potvrzuje i fakt, že na úrovni obcí je stále patrný _„vliv poválečného odsunu původního německého obyvatelstva s následné osídlování pohraničí“_. Svou stopu zanechalo i rozsáhlé stěhování za prací do oblastí těžby uhlí a těžkého průmyslu. Právě proto jsou v mapě snadno odlišitelné oblasti někdejších [Sudet](https://interaktivni.rozhlas.cz/sudety/).

O vyhnání sudetských Němců mluví i [Ivana Přidalová](http://urrlab.cz/cs/profil/ivana-pridalova) a [Petra Špačková](http://urrlab.cz/cs/profil/petra-spackova) z Přírodovědecké fakulty Univerzity Karlovy. 

Mimo pohraničí pak hraje roli jak suburbanizace, tedy stěhování obyvatel do okolí velkých měst. Podle demografek to _„souvisí jednak s dostupností práce ve velkém městě a jednak s dostupností bydlení v jeho zázemí,“_ což je možné pozorovat zejména v okolí Prahy. Ideální příklad jsou satelitní sídliště v okolí hlavního města, jako třeba Černošice či Říčany. Rodáky je zde akorát nejmladší generace, tedy děti dnešních třicátníků a čtyřicátníků, které se zde narodily.

_„Rozdíly v ochotě zůstat, respektive opustit rodnou obec, budou spojené jak s dostupností práce, tak také s kulturou a regionální identitou,“_ dodávají demografky z univerzity. Jde tak o _„nezakořeněné obyvatelstvo pohraničí“_ na straně jedné, a _„stabilní populaci jižní Moravy a Vysočiny se silnějším dodržování tradic a možná i náboženství“_ na straně druhé.